<?php




//loading code 
require_once("config.php");
require_once("code/db_interface.php");
require_once("code/reservation_interface.php");
require_once("code/activities_interface.php");
require_once("code/user_interface.php");

require_once("code/activity.php");
require_once("code/user.php");
require_once("code/reservation.php");
require_once("code/tool.php");


session_start();

if(!isset($_SESSION[SESSION_LOGGED])){
	$_SESSION[SESSION_LOGGED] = false;
}
if(isset($_SESSION[SESSION_LOGGED]) &&  $_SESSION[SESSION_LOGGED] == true){
	Tool::startSecureConnection();
}


//load main-page layout
require_once("design/page.php");