<?php


define("ACTIVITIES_TABLE", "activities");

define("ACTIVITY_NAME", "activity_name");
define("ACTIVITY_MAX_AVAILABILITY", "max_availability");
define("ACTIVITY_COUNT_RESERVATIONS", "count_reservations");


/**
* 
*/
class ActivitiesInterface
{

	private $db;

	function __construct(){
		$this->db = DbInterface::dbOpenConnection();
	}

	function __destruct(){
		DbInterface::dbCloseConnection();
	}
	

	function load($activityName , $update = false){

		$activityName = $this->db->real_escape_string($activityName);

		$query = 'SELECT * FROM ' . ACTIVITIES_TABLE . ' WHERE ' . ACTIVITY_NAME .' LIKE "' . $activityName . '"';

		if($update){
			$query .= " FOR UPDATE";
		}

		$res=$this->db->query($query);

		if ($res) {
			if ($res->num_rows == 1) {
				while($row = $res->fetch_assoc()){

					$activity = new Activity($row[ACTIVITY_NAME], $row[ACTIVITY_MAX_AVAILABILITY], $row[ACTIVITY_COUNT_RESERVATIONS]); 
				}
				
			} else if($res->num_rows > 1){
				throw new Exception("Error loading activity[". $activityName ."]. Activity doesn't exist or too many.", 1);
				
			} else {
				return false;
			}
		} else {
			return false;	
		}
		return $activity;	
	}

	function getAll(){
		$activities = array();

		$query = 'SELECT ' . ACTIVITY_NAME . ' FROM ' . ACTIVITIES_TABLE .' ORDER BY (' . ACTIVITY_MAX_AVAILABILITY .' - ' . ACTIVITY_COUNT_RESERVATIONS . ') DESC ';

		$res=$this->db->query($query);

		if ($res) {
			while($row = $res->fetch_assoc()){
				$activities[] = $row[ACTIVITY_NAME];
			}
		} else {
			return false;	
		}
		return $activities;	
	}


	function update($activity){

		if(!is_object($activity)){
			throw new Exception("Error updating activity, invalid argument type", 1);
		}

		if(!$activity->getAsserted()){
			throw new Exception("Error updating activity, assert before", 1);
		}

		$activityName = $this->db->real_escape_string($activity->getActivityName());
		$countReservations = $this->db->real_escape_string($activity->getCountReservations());

		if(!is_numeric($countReservations)){
			throw new Exception("Count reservation must be a numeric value", 1);
		}

		$query = 'UPDATE ' . ACTIVITIES_TABLE . ' SET ' . ACTIVITY_COUNT_RESERVATIONS . ' = ' . $countReservations . ' WHERE ' . ACTIVITY_NAME . ' LIKE "' . $activityName . '" ;';

		$res = $this->db->query($query);

		if(!$res){
			throw new Exception("Error updating " . ACTIVITIES_TABLE . " table", 1);
		}
	}
	
}

?>









