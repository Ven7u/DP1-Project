<?php

/**
* 
*/
class DbInterface{

	private static $istance = null;
	private static $db = null;
	private static $count;

	private function __construct(){
		self::$db = new mysqli(DB_HOST, DB_USER, DB_PWD, DB_NAME );

		if (mysqli_connect_error()) {
			die('Connection Error (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
		}	
	}

	// public function __destruct(){
	// 	$this->dbCloseConnection();
	// }

	public static function dbOpenConnection(){

		if(self::$istance == null){
			self::$istance = new DbInterface();
			self::$count = 0;
		}
		
		self::$count++;

		return self::$db;
	}

	public static function dbCloseConnection(){
		self::$count--;

		if(self::$count == 0){
			self::$db->close();
		}
		
	}

	public static function dbAutoCommit($mode = false){
		self::dbOpenConnection();
		return self::$db->autocommit($mode);
	}
	public static function dbCommit(){
		if(self::$db == null){
			throw new Exception("Can't rollback, first open a connection", 1);
		}
		if(! self::$db->commit()){
			throw new Exception("Error on commit", 1);
			
		}
	}
	public static function dbRollback(){
		if(self::$db == null){
			throw new Exception("Can't rollback, first open a connection", 1);
		}

		if(!self::$db->rollback()){
			throw new Exception("Error on rollback", 1);
			
		}
	}



}
?>