<?php

define("RESERVATIONS_TABLE", "reservations");

define("RESERVATION_ACTIVITY", "reservation_activity");
define("RESERVATION_USER", "reservation_user");
define("RESERVATION_COUNT_UNDER_AGE", "count_under_age");

/**
* 
*/
class ReservationInterface  
{
	private $db;

	function __construct(){
		$this->db = DbInterface::dbOpenConnection();
	}

	function __destruct(){
		DbInterface::dbCloseConnection();
	}

	function load($activityName,$username, $update = false){

		$activityName = $this->db->real_escape_string($activityName);
		$username = $this->db->real_escape_string($username);

		$query = 'SELECT * FROM ' . RESERVATIONS_TABLE . ' WHERE ' . RESERVATION_ACTIVITY .' LIKE "' . $activityName . '" AND ' . RESERVATION_USER . ' LIKE "' . $username . '"';

		if($update){
			$query .= " FOR UPDATE";
		}

		$res=$this->db->query($query);

		if ($res) {
			if ($res->num_rows == 1) {
				while($row = $res->fetch_assoc()){ 
					$reservation = new Reservation($row[RESERVATION_ACTIVITY], $row[RESERVATION_USER] , $row[RESERVATION_COUNT_UNDER_AGE]);
				}
				
			} else if($res->num_rows > 1){
				throw new Exception("Error loadig reservation for user [". $username ."]. Reservation doesn't exist or too many.", 1);	
			}
			else {
				return false;
			}
		} else {
			throw new Exception("Error loadin user[". $username ."].", 1);
					
		}
		return $reservation;
	}

	function getAllByUser($username){
		$reservedActivities = array();
		$username = $this->db->real_escape_string($username);

		$query = 'SELECT ' . RESERVATION_ACTIVITY . ' FROM ' . RESERVATIONS_TABLE .' WHERE ' . RESERVATION_USER .' LIKE "' . $username . '" ;';

		$res=$this->db->query($query);

		if ($res) {
			while($row = $res->fetch_assoc()){
				$reservedActivities[] = $row[RESERVATION_ACTIVITY];
			}
		} else {
			throw new Exception("Error getting reserved activities fo [$username]", 1);
					
		}
		return $reservedActivities;	
	}


	function insertNew($reservation){

		if(!is_object($reservation)){
			throw new Exception("Error creating new reservation on db, invalid argument type", 1);
		}

		if(!$reservation->getAsserted()){
			throw new Exception("Error creating new reservation, assert before", 1);
		}

		$activityName = $this->db->real_escape_string($reservation->getActivityName());
		$username = $this->db->real_escape_string($reservation->getUsername());
		$countUnderAge = $this->db->real_escape_string($reservation->getCountUnderAges());
		if(!is_numeric($countUnderAge)){
			throw new Exception("Invalid value [$countUnderAge]", 1);	
		}

		$query = 'INSERT INTO ' . RESERVATIONS_TABLE . '( ' . RESERVATION_ACTIVITY . ' , ' . RESERVATION_USER . ' , ' . RESERVATION_COUNT_UNDER_AGE . ' ) 
					VALUES ( "' . $activityName . '" , "' . $username . '" , "' . $countUnderAge .'" );';

		if(!$this->db->query($query)){
			throw new Exception("Error creating new reservation for [$activityName] [$username] on db", 1);
		}
	}

	function delete($reservation){

		if(!is_object($reservation)){
			throw new Exception("Error creating new reservation on db, invalid argument type", 1);
		}

		if(!$reservation->getAsserted()){
			throw new Exception("Error creating new reservation, assert before", 1);
		}

		$activityName = $this->db->real_escape_string($reservation->getActivityName());
		$username = $this->db->real_escape_string($reservation->getUsername());
		$countUnderAge = $this->db->real_escape_string($reservation->getCountUnderAges());

		if(!is_numeric($countUnderAge)){
			throw new Exception("Invalid value [$countUnderAge]", 1);	
		}

		$query = 'DELETE FROM ' . RESERVATIONS_TABLE . ' WHERE ' . RESERVATION_USER . ' LIKE "' . $username . '" AND ' . RESERVATION_ACTIVITY . ' LIKE "' . $activityName . '" ;';

		if(!$this->db->query($query)){
			throw new Exception("Error creating new reservation for [$activityName] [$username] on db", 1);
		}

	}

}
