<?php 

define("ACCEPTED_VALUE_REGEX", "/[\w]+/i");
define("SALT_BYTE_SIZE", "24");
/**
* 
*/
class User
{
	

	private $username;
	private $password;
	private $saltCode = false;
	private $asserted = false;


	function __construct($username , $password){
		//TODO parsing username
		$this->username = $username;
		$this->password = $password;
	}


	function getUsername(){
		return $this->username;
	}

	function getPassword(){
		return $this->password;
	}

	function setUsername($username){
		//TODO
	}

	function setPassword($password){
		//TODO
	}

	function getAsserted(){
		return $this->asserted;
	}

	function getSaltCode(){
		if($this->saltCode){
			return $this->saltCode;
		}

		$this->saltCode = base64_encode(mcrypt_create_iv(SALT_BYTE_SIZE, MCRYPT_DEV_URANDOM));
		return $this->saltCode;
	}

	function setSaltCode($saltCode){
		if(!$this->saltCode){
			$this->saltCode = $saltCode;
			return true;	
		}
		return false;
	}



	function assertUser(){

		$this->username = strip_tags($this->username);
		$this->password = strip_tags($this->password);
		$this->username = strval($this->username);
		$this->password = strval($this->password);

		if(strlen($this->username)==0){
			throw new Exception("Username is required", 1);
		}
		if(strlen($this->password) == 0){
			throw new Exception("Password is required", 1);
		}


		$tmp = preg_replace( ACCEPTED_VALUE_REGEX , "" , $this->username );
		if(!empty($tmp)){
			throw new Exception("Invalid username", 1);
		}

		$tmp = preg_replace( ACCEPTED_VALUE_REGEX , "" , $this->password );
		if(!empty($tmp)){
			throw new Exception("Invalid password", 1);
		}

		$this->asserted = true;
	}

	function encodePassword(){
		if(!$this->asserted){
			throw new Exception("User not yet asserted", 1);
		}

		$salt = $this->getSaltCode();

		$this->password = md5($this->password . $salt);
	}

	function subscribe(){
		
		DbInterface::dbAutoCommit(false);

		$this->assertUser();

		$this->encodePassword();

		$userInterface = new UserInterface();

		try{

		if($userInterface->load($this->username)){
			throw new Exception("Username [$this->username] already exist", 1);	
		}

			$userInterface->insertNew($this);
			DbInterface::dbCommit(); 
		} catch (Exception $e){
			DbInterface::dbRollback();
			throw new Exception("Error processing user subscription " . $e->getMessage(), 1);
			
		}


		
	}

	function rebuildPassword($salt){
		if(!$this->asserted){
			throw new Exception("User not yet asserted", 1);
		}

		$this->password = md5($this->password . $salt);
	}

	function login(){

		$this->assertUser();

		$userInterface = new UserInterface();
		
		if(!($correspondingUser = $userInterface->load($this->username))){
			throw new Exception("Username [$this->username] doesn't exist", 1);
		}

		$this->rebuildPassword($correspondingUser->getSaltCode());

		if($correspondingUser->getPassword() !== $this->password){
			throw new Exception("Wrong password", 1);
			
		}
		return true;
	}


}









