<?php

define("MAX_UNDER_AGES", "3");

/**
* 
*/
class Reservation
{	
	private $activityName;
	private $username;
	private $countUnderAges;
	private $asserted = false;
	
	function __construct($activityName, $username , $countUnderAges){
		$this->activityName = $activityName;
		$this->username = $username;
		$this->countUnderAges = $countUnderAges;
	}

	function getActivityName(){
		return $this->activityName;
	}

	function getUsername(){
		return $this->username;
	}
	function getCountUnderAges(){
		return $this->countUnderAges;
	}

	function getBooked(){
		return $this->countUnderAges + 1;
	}

	function getAsserted(){
		return $this->asserted;
	}

	function book(){

		DbInterface::dbAutoCommit(false);

		$userInterface = new UserInterface();
		$activityInterface = new ActivitiesInterface();
		$reservationInterface = new ReservationInterface();

		try{

		if($reservationInterface->load($this->activityName, $this->username)){
		 	throw new Exception("Reservation for [$this->activityName] already exist", 1);
		}


		
		if(!$userInterface->load($this->username)){
			throw new Exception("User with username [$this->username] doesn't exist", 1);
		}
		
		$activity = $activityInterface->load($this->activityName, true);
		if(!$activity){
			throw new Exception("Activity [$this->activityName] doesn't exist", 1);
		}

		if(!is_numeric($this->countUnderAges)){
			throw new Exception("Invalid value for under ages", 1);
		}

		$this->countUnderAges = intval($this->countUnderAges);

		if($this->countUnderAges < 0 || $this->countUnderAges > MAX_UNDER_AGES){
			throw new Exception("Too many under ages.", 1);
		}

		$count = $this->countUnderAges + 1;

		
			$activity->book($count);
		} catch (Exception $e){
			throw new Exception($e->getMessage(), 1);
			
		}

		$this->asserted = true;

		try{
			$reservationInterface->insertNew($this);
			$activityInterface->update($activity);
		} catch (Exception $e){
			DbInterface::dbRollback();
			throw new Exception($e->getMessage(), 1);
			
		}

		DbInterface::dbCommit();
	}

	function delete(){
		DbInterface::dbAutoCommit(false);

		$userInterface = new UserInterface();
		$activityInterface = new ActivitiesInterface();
		$reservationInterface = new ReservationInterface();

		// if($reservationInterface->load($this->activityName, $this->username)){
		// 	throw new Exception("Reservation for [$this->activityName] already exist", 1);
		// }

		try{

		if(!$userInterface->load($this->username)){
			throw new Exception("User with username [$this->username] doesn't exist", 1);
		}
		
		$activity = $activityInterface->load($this->activityName, true);
		if(!$activity){
			throw new Exception("Activity [$this->activityName] doesn't exist", 1);
		}

		if(!is_numeric($this->countUnderAges)){
			throw new Exception("Invalid value for under ages", 1);
		}

		$this->countUnderAges = intval($this->countUnderAges);

		// if($this->countUnderAges < 0 || $this->countUnderAges > MAX_UNDER_AGES){
		// 	throw new Exception("Too many under ages.", 1);
		// }

		$count = $this->countUnderAges + 1;

		
			$activity->removeBook($count);
		} catch (Exception $e){
			throw new Exception($e->getMessage(), 1);
			
		}

		$this->asserted = true;

		try{
			$reservationInterface->delete($this);
			$activityInterface->update($activity);
		} catch (Exception $e){
			DbInterface::dbRollback();
			throw new Exception($e->getMessage(), 1);
			
		}

		DbInterface::dbCommit();

	}
}







