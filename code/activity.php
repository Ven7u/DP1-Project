<?php 


/**
* 
*/
class Activity
{
	private $activityName;
	private $maxAvailability;
	private $countReservations; 
	private $asserted = false;


	function __construct($activityName ,$maxAvailability ,$countReservations){
		
		$this->activityName = $activityName;
		$this->maxAvailability = $maxAvailability;
		$this->countReservations = $countReservations;
	}

	function getActivityName(){
		return $this->activityName;
	}

	function getMaxAvailability(){
		return $this->maxAvailability;
	}
	function getCountReservations(){
		return $this->countReservations;
	}

	function getAsserted(){
		return $this->asserted;
	}

	function setActivityName(){
		//TODO	
	}

	function setMaxAvailability(){
		//TODO
	}
	function setCountReservations(){
		//TODO
	}

	function book($count){

		if(is_numeric($count)){
			if(($count + $this->countReservations) <=  $this->maxAvailability ){
				$this->countReservations += $count;
				$this->asserted = true ; 
			} else {
				throw new Exception("No more space for [$count] reservations", 1);	
			}
		} else {
			throw new Exception("Invalid value [$count] for activity reservation", 1);
			
		}
	}

	function removeBook($count){
		if(is_numeric($count)){
			if(( $this->countReservations - $count ) >=  0 ){
				$this->countReservations -= $count;
				$this->asserted = true ; 

			} else {
				throw new Exception("Error removing [$count] reservations", 1);	
			}
		} else {
			throw new Exception("Invalid value [$count] for activity reservation", 1);
			
		}
	}
}










