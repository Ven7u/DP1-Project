<?php

define("USER_TABLE", "users");

define("USERNAME", "username");
define("PASSWORD", "password");
define("SALT_CODE", "salt_code");

/**
* 
*/
class UserInterface 
{

	private $db;

	function __construct(){
		$this->db = DbInterface::dbOpenConnection();
	}

	function __destruct(){
		DbInterface::dbCloseConnection();
	}

	function load($username){

		$user;

		$username = $this->db->real_escape_string($username);

		$query = 'SELECT * FROM ' . USER_TABLE . ' WHERE ' . USERNAME .' LIKE "' . $username . '"';

		$res=$this->db->query($query);

		if ($res) {
			if ($res->num_rows == 1) {
				while($row = $res->fetch_assoc()){ 
					$user = new User($row[USERNAME], $row[PASSWORD]);
					$user->setSaltCode($row[SALT_CODE]);
				}
				
			} else if( $res->num_rows > 1){
				throw new Exception("Error loading user[". $username ."]. Username doesn't exist or too many.", 1);
				
			} else {
				return false;
			}
		} else {
			throw new Exception("Error loading user[". $username ."].", 1);
		}
		return $user;
	}

	function insertNew($user){

		if(!is_object($user)){
			throw new Exception("Error creating new user on db, invalid argument type", 1);
		}

		if(!$user->getAsserted()){
			throw new Exception("Error creating new user on db, assert before", 1);
		}

		$username = $this->db->real_escape_string($user->getUsername());
		$password = $this->db->real_escape_string($user->getPassword());
		$saltCode = $this->db->real_escape_string($user->getSaltCode());

		$query = 'INSERT INTO ' . USER_TABLE . '( ' . USERNAME . ' , ' . PASSWORD . ' , ' . SALT_CODE . ' ) 
					VALUES ( "' . $username . '" , "' . $password . '" , "' . $saltCode .'" );';

		if(!$this->db->query($query)){
			throw new Exception("Error creating new user on db", 1);
		}

		return true;
	}

}

