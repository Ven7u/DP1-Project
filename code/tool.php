<?php

define("PAGE_URL_REGEX", "/[a-z_]+\.php$/");
define("PHP_REPLACE_REGEX","/\.php/");
/**
* 
*/
class Tool
{
	

	static function generateFormToken($form) {
       // generate a token from an unique value
		$token = md5(uniqid(microtime(), true));  

    	// Write the generated token to the session variable to check it against the hidden field when the form is sent
		$_SESSION[$form.'_token'] = $token; 


		return $token;

	}

	static function verifyFormToken($form) {
    // check if a session is started and a token is transmitted, if not return an error
		if(!isset($_SESSION[$form.'_token'])) { 
			return false;
		}

	// check if the form is sent with token in it
		if(!isset($_POST['token'])) {
			return false;
		}
	// compare the tokens against each other if they are still the same
		if ($_SESSION[$form.'_token'] !== $_POST['token']) {
			return false;
		}

		return true;
	}

	static function startSecureConnection(){
		if(!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == ""){
			$redirect = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
			header("HTTP/1.1 301 Moved Permanently");
			header("Location: $redirect");

		}
	}

	static function stopSecureConnection(){
		if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on"){
			$redirect = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
			header("HTTP/1.1 301 Moved Permanently");
			header("Location: $redirect");

		}
	}

	static function urlRedirect($page, $secure = false){

		$url = 'Location: ';

		if(!$secure){
			$url .= 'http://'; 
		} else {
			$url .= 'https://';
		}

		$url .= $_SERVER['HTTP_HOST'] . '/';
		$url .= FOLDER;
		$url .= $page;

		header($url);
		exit();
	}

	static function getUrl($page, $secure = false){
		$url = "";
		if(!$secure){
			$url .= 'http://'; 
		} else {
			$url .= 'https://';
		}

		$url .= $_SERVER['HTTP_HOST'] . '/';
		$url .= FOLDER;
		$url .= $page;
		return $url;
	}

	//deprecated
	static function checkCookieOn(){
		

		setcookie("test_cookie", true);
		if(!isset($GLOBALS['cookie-test']) ){
			$GLOBALS['cookie-test'] = true;
			header("Local: ".$_SERVER['REQUEST_URI']);
			
		}

		if(!isset($_COOKIE['test_cookie']) || $_COOKIE['test_cookie'] != true ){
			$GLOBALS['cookie-test'] = false;
			require_once('design/cookie-disabled.php');
			exit();
		}

	}

	static function assertUrl($url, $secure = false){

		$newUrl= 'index.php';

		$matches = array();

		if(preg_match(PAGE_URL_REGEX, $url, $matches)){
			if($matches[0] != 'index.php'){
				$newUrl .= "?id=";
				$newUrl .= preg_replace(PHP_REPLACE_REGEX,"", $matches[0]);
				Tool::urlRedirect($newUrl);
			}
		}
	}

}




