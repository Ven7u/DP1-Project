	function checkLogin(){

		$("#login-form").submit( function( event ) {
			if($("input[name=username]").val().match(/[\W]+/i) || $("input[name=username]").val() == ""){
				event.preventDefault();
				$("small#username-alert").css("display","block");
				$("#login-form").find("#username-group").addClass("has-error");
			} else {
				$("small#username-alert").css("display","none");
				$("#login-form").find("#username-group").removeClass("has-error");

			}
			if($("input[name=password]").val().match(/[\W]+/i) || $("input[name=password]").val() == ""){
				event.preventDefault();
				$("small#password-alert").css("display","block");
				$("#login-form").find("#password-group").addClass("has-error");
			}
			else {
				$("small#password-alert").css("display","none");
				$("#login-form").find("#password-group").removeClass("has-error");

			}

		});
	}

	function checkSubscribe(){

		$("#subscribe-form").submit( function( event ) {
			if($("input[name=username]").val().match(/[\W]+/i) || $("input[name=username]").val() == ""){
				event.preventDefault();
				$("small#username-alert").css("display","block");
				$("#subscribe-form").find("#username-group").addClass("has-error");
			} else {
				$("small#username-alert").css("display","none");
				$("#subscribe-form").find("#username-group").removeClass("has-error");

			}
			if($("input[name=password]").val().match(/[\W]+/i) || $("input[name=password]").val() == ""){
				event.preventDefault();
				$("small#password-alert").css("display","block");
				$("#subscribe-form").find("#password-group").addClass("has-error");
			}
			else {
				$("small#password-alert").css("display","none");
				$("#subscribe-form").find("#password-group").removeClass("has-error");

			}

		});
	}
