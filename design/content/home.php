<?php
require_once(__DIR__ . '/../../config.php');
require_once(__DIR__ . '/../../code/tool.php');
Tool::assertUrl($_SERVER['REQUEST_URI']);	


if(!isset($_SESSION[SESSION_LOGGED]) ||  $_SESSION[SESSION_LOGGED] != true){
	Tool::stopSecureConnection(); 
}
?>


<div class="home-title">
	<h1 class="home-title">Available activities</h1>
</div>

<?php 
if(isset($_GET['error-msg'])){ 
	$_GET['error-msg'] = strip_tags($_GET['error-msg']);
	?>
<div class="col-sm-12 alert alert-danger">
	<?php echo $_GET['error-msg']; 
	unset($_GET['error-msg']);
	?>


</div>


<?php }?>

<?php

$activityInterface = new ActivitiesInterface();
$activityNames = $activityInterface->getAll();


foreach ($activityNames as $activityName) {
	$activity = $activityInterface->load($activityName);
	?>
	<div class=" activity-wrapper col-sm-4">
		<div class="activity-name">
			<h4 ><?= $activity->getActivityName() ?></h4>
		</div>
		<div class="activity-info">
			<p>Total places: <?php echo $activity->getMaxAvailability() ?></p>
			<p>Already booked: <?php echo $activity->getCountReservations() ?></p>
		</div>
		
	</div>
	<?php } ?>