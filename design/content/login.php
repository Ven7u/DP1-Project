<?php
require_once(__DIR__ . '/../../config.php');
require_once(__DIR__ . '/../../code/tool.php');
Tool::assertUrl($_SERVER['REQUEST_URI']);

if(Tool::verifyFormToken('login')){
	if(isset($_POST['username']) && isset($_POST['password']) ){
		$user = new User($_POST['username'], $_POST['password']);
		
		try{
			if($user->login()){
				$_SESSION[SESSION_USERNAME] = $user->getUsername();
				$_SESSION[SESSION_LOGGED] = true;
				var_dump($_SESSION[SESSION_USERNAME] );
				var_dump($_SESSION[SESSION_LOGGED]);
				Tool::urlRedirect("index.php?id=user_page",true);
			}

		} catch(Exception $e){
			$errorMsg =  "ERROR: " . $e->getMessage();
		} 
	}

}

Tool::startSecureConnection();

$token = Tool::generateFormToken('login');

?>

<div class="col-sm-offset-2 col-sm-4">
	
	<h2 class="text-center text-uppercase">Login</h2>
	<p class = "text-justify">Insert username and password and get access to your private area.</p>

	<?php if(isset($errorMsg)){?>
			<div class="alert alert-danger" role="alert">
				<?php echo $errorMsg; ?>
			</div>
	<?php } ?>

	<form class="form-signin" id="login-form" action="index.php?id=login" name="login" method="POST">
	<div class="form-group" id="username-group">
		<label for="username">Username</label>
		<input class="form-control" type="text" name="username" placeholder="Username">
		<small class="alert alert-danger " id="username-alert" style="display:none;"> Username non valido, inserire solo valori alfanumerici o " _ " </small>
	</div>
	<div class="form-group" id="password-group">
		<label for="password">Password</label>
		<input class="form-control" type="password" name="password" placeholder="Password">
		<small class="alert alert-danger " id="password-alert" style="display:none;"> Password non valida, inserire solo valori alfanumerici o " _ " </small>
	</div>

	<input type="hidden" name="token" value=<?php echo '"' . $token . '"'; ?> >

	<input class="btn btn-default btn-block" type="submit" name="submit" value="Accedi"> 
</form>
</div>




<script> checkLogin(); </script>





