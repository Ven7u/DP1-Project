<?php 
require_once(__DIR__ . '/../../config.php');
require_once(__DIR__ . '/../../code/tool.php');
Tool::assertUrl($_SERVER['REQUEST_URI']);


if(!isset($_SESSION[SESSION_USERNAME]) || !$_SESSION[SESSION_LOGGED]){
	Tool::urlRedirect("index.php");
}

if (isset($_SESSION[SESSION_TIME])){
	$t=time();
	$t0=$_SESSION[SESSION_TIME]; 
	$diff=($t-$t0);


	if ($diff > SESSION_TIMEOUT) {
		if (ini_get("session.use_cookies")) { // PHP using cookies to handle session
			$params = session_get_cookie_params();
			setcookie(session_name(), '', time() - 3600*24, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
		} 
		Tool::urlRedirect("index.php?id=logout&error-msg=TimeExpired");
	} else {
		$_SESSION[SESSION_TIME]=time();
	}
} else {
	$_SESSION[SESSION_TIME]=time();
}


try{
	$userInterface = new UserInterface();
	$user = $userInterface->load($_SESSION[SESSION_USERNAME]);
	if(!$user){
		Tool::urlRedirect("index.php");
	}
} catch (Exception $e){
	Tool::urlRedirect("index.php");
}

try{
	$activityInterface = new ActivitiesInterface();
	$activities = $activityInterface->getAll();
} catch (Exception $e){
	echo '<div class="alert alert-danger"> ERROR: ' . $e->getMessage() . ' </div>';
	exit();
}


if(Tool::verifyFormToken('book')){

	if(isset($_POST['activityName']) && isset($_POST['underAges']) ){
		$newReservation = new Reservation($_POST['activityName'], $user->getUsername(), $_POST['underAges']);

		try{
			$newReservation->book();		
			echo '<div class="alert alert-success"> BOOKED SUCCESSFULLY </div>';
		} catch(Exception $e){
			echo '<div class="alert alert-danger"> ERROR: ' . $e->getMessage() . ' </div>';
		} 
	}

} else if(isset($_POST['username']) && isset($_POST['activityName'])){
	$formName = $_POST['username'] . '-' . $_POST['activityName'];
	if(Tool::verifyFormToken($formName)){

		try{

			Tool::generateFormToken( $formName);
			$reservationInterface = new ReservationInterface();
			$reservation = $reservationInterface->load($_POST['activityName'],$_POST['username']);
			$reservation->delete();
			echo '<div class="alert alert-success"> DELETED SUCCESSFULLY </div>';
		} catch (Exception $e){
			echo '<div class="alert alert-danger"> ERROR: ' . $e->getMessage() . ' </div>';
		}
	}
}



$token = Tool::generateFormToken('book');

?>

<div class="user-title">
	<h2>HELLO <?= $user->getUsername(); ?></h2>
</div>
<div class="user-book">

	<p class="lead">Select the activity and book for you and for your children:</p>

	<form class="form-inline" action="index.php?id=user_page" method="POST" name="book">
		<div class="form-group">
			<label for="activityName">Activity name</label>
			<select class="form-control" name = "activityName">
				<?php foreach($activities as $activity) { ?>
				<option value="<?= $activity ?>"><?= $activity ?></option>
				<?php } ?>
			</select>
		</div>

		<div class="form-group">
			<label for="underAges">Children</label>
			<select class="form-control" name="underAges">

				<option value="0">0</option>
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>

			</select>
		</div>


		<input type="hidden" name="token" value=<?php echo '"' . $token . '"'; ?> >
		<input type="hidden" name="mode" value="book" >

		<input class="btn btn-default" type="submit" value="Book">
	</form>
	
</div>




<h3>Prenotazioni attive</h3>

<div class="row">
	<div class="col-sm-10">

		<?php 
		$reservations = array();
		$reservationInterface = new ReservationInterface();
		try{
			$activitiesReserved = $reservationInterface->getAllByUser($user->getUsername());
		} catch (Exception $e){
			echo $e->getMessage();
		}

		foreach ($activitiesReserved as $activityName) {
			try{ 
				$reservations[] = $reservationInterface->load($activityName,$user->getUsername());
			} catch (Exception $e) {
				throw new Exception("Error loading reservation" . $e->getMessage(), 1);
			}
		}
		if(!empty($reservations)){ ?>
		<table class="table table-striped booked-activities">
			<?php
			foreach ($reservations as $reservation) {
				$token = Tool::generateFormToken( $reservation->getUsername() . '-' . $reservation->getActivityName() );
				?>
				<tr>
					<td>
						<p class="form-control-static"><?php echo $reservation->getActivityName() . ": " . $reservation->getBooked();?></p>
					</td>
					<td>
						<form class="form-inline" action= <?php echo '"' . Tool::getUrl("index.php?id=user_page") . '"'; ?>   method="POST" name=<?php echo '"' . $reservation->getUsername() . '-' . $reservation->getActivityName() . '"'?> >	
							<input type="hidden" name="username" value=<?php echo '"' . $reservation->getUsername() . '"'?> >
							<input type="hidden" name="activityName" value=<?php echo '"' . $reservation->getActivityName() . '"'?> >
							<input type="hidden" name="token" value= <?php echo '"' . $token . '"'?>>
							<input class="btn btn-default" type="submit" value="Delete">
						</form>
					</td>
				</tr>
				<?php
			} ?>
		</table>
		<?php
	} else {
		?>
		<h4>Nessuna prenotazione attiva</h4>

		<?php } ?>

	</div>
</div>













