
<?php

require_once(__DIR__ . '/../../config.php');
require_once(__DIR__ . '/../../code/tool.php');
Tool::assertUrl($_SERVER['REQUEST_URI']);

if(Tool::verifyFormToken('subscribe')){
	if(isset($_POST['username']) && isset($_POST['password']) ){
		$newUser = new User($_POST['username'], $_POST['password']);
		
		try{
			$newUser->subscribe();
			$successMsg =  "WELCOME: " . $newUser->getUsername();

		} catch(Exception $e){
			 $errorMsg = "ERROR: " . $e->getMessage();
		} 
	}

}

$token = Tool::generateFormToken('subscribe');

Tool::startSecureConnection();

?>

<div class="col-sm-offset-2 col-sm-4">

	<h2 class="text-center text-uppercase">Subscribe</h2>
	<p class = "text-justify">Insert username and password and subscribe now.</p>

	<?php if(isset($errorMsg)){?>
			<div class="alert alert-danger" role="alert">
				<?php echo $errorMsg; ?>
			</div>
	<?php } elseif (isset($successMsg)) { ?>
			<div class="alert alert-success" role="alert">
				<?php echo $successMsg; ?>
			</div>
	<?php }?>

	<form action="index.php?id=subscribe" id="subscribe-form" name="subscribe" method="POST">
		<div class="form-group" id="username-group">
			<label for="username">Username</label>
			<input class="form-control" type="text" name="username" placeholder="Username">
			<small class="alert alert-danger " id="username-alert" style="display:none;"> Username non valido, inserire solo valori alfanumerici o " _ " </small>
		</div>
		<div class="form-group" id="password-group">
			<label for="password">Password</label>
			<input class="form-control" type="password" name="password" placeholder="Password">
			<small class="alert alert-danger " id="password-alert" style="display:none;"> Password non valida, inserire solo valori alfanumerici o " _ " </small>
		</div>
		<input type="hidden" name="token" value=<?php echo '"' . $token . '"'; ?> >

		<input class="btn btn-default btn-block" type="submit" name="submit" value="Iscriviti"> 
	</form>

</div>

<script> checkSubscribe(); </script>





