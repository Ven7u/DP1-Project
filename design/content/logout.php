<?php 
require_once(__DIR__ . '/../../config.php');
require_once(__DIR__ . '/../../code/tool.php');
Tool::assertUrl($_SERVER['REQUEST_URI']);

if(isset($_GET['error-msg'])){
	$errorMsg = "?error-msg=" . $_GET['error-msg'];
} else {
	$errorMsg = "";
}


session_destroy(); 
Tool::urlRedirect("index.php" . $errorMsg);
?>