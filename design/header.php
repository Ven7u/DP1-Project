<?php
require_once(__DIR__ . '/../config.php');
require_once(__DIR__ . '/../code/tool.php');
Tool::assertUrl($_SERVER['REQUEST_URI']);
?>

<div class="header-container">
	<div class="container-fluid top-header">
		<div class="row">
			<div class="col-sm-offset-1 header-title">
				<h1>Sistema di prenotazione attivit&agrave;</h1>
			</div>
		</div>
	</div>
	<div class="container-fluid header-subtitle">
		<div class="row">
			<div class=" col-sm-offset-1 col-sm-8">
				<h4>Iscriviti e prenota la tua attivit&agrave; ora!</h4>
			</div>
			<div class="col-sm-3">
				<h4><?php echo (isset($_SESSION[SESSION_USERNAME]) && isset($_SESSION[SESSION_LOGGED]) && $_SESSION[SESSION_LOGGED] == true) ? "Benvenuto " . $_SESSION[SESSION_USERNAME] : ""; ?> </h4>
			</div>
		</div>
	</div>
</div>