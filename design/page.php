<?php 
require_once(__DIR__ . '/../config.php');
require_once(__DIR__ . '/../code/tool.php');
Tool::assertUrl($_SERVER['REQUEST_URI']);
?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href= "css/bootstrap.min.css" >
	<link rel="stylesheet" href= "css/style.css" >
	<script src="js/jquery-1.11.3.min.js"></script>
	<script src="js/script.js"></script>
</head>
<body>
<div id="main-container">
	<noscript class="container-fluid alert alert-warning">
			Warning - javascript is disabled - the website could not work properly, please enable
	</noscript>	



	<script type="text/javascript">
		if (!navigator.cookieEnabled) {
			window.location.href = 'coockie-disabled';
		}

		document.cookie = "cookietest";
		var ret = (document.cookie.indexOf("cookietest") != -1)? true: false;

		document.cookie = "cookietest; expires=Thu, 01-Jan-1970 00:00:01 GMT";
		if(!ret){
			window.location.href = 'cookie-disabled.php';
		}
	</script>



	<header>

		<?php include("header.php"); ?>
	</header>
	<div class="container" >


		<div class="col-sm-2">
			<?php include("nav.php"); ?>
		</div>
		<div class="col-sm-10">
			<div class="col-sm-offset-1"> 
				<?php include("content.php"); ?>
			</div>
		</div>

	</div>
	</div>
	<footer>
		<?php include("footer.php"); ?>
	</footer>



	
</body>
</html>







