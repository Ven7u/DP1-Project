<?php

require_once(__DIR__ . '/../config.php');
require_once(__DIR__ . '/../code/tool.php');
Tool::assertUrl($_SERVER['REQUEST_URI']);

define("CONTENT", "design/content/");

if(isset($_GET['id'])){
	$_GET['id'] = strip_tags($_GET['id']);
}

if(isset($_GET['id']) && file_exists(CONTENT . $_GET['id'] . '.php' )){
	include(CONTENT . $_GET['id'] . '.php'); 
} else {
	include(CONTENT.'home.php');
}

?>